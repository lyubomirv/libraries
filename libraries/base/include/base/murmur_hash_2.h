//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cstddef>
#include <stdint.h>

namespace base
{
namespace murmur_2
{

/*!
 * \brief Murmur hash 2 32-bit.
 * \note This code makes a few assumptions about how your machine behaves:
 *   1. We can read a 4-byte value from any address without crashing.
 * \note And it has a few limitations:
 *   1. It will not work incrementally;
 *   2. It will not produce the same results on little-endian and big-endian machines.
 *
 * Source: https://sites.google.com/site/murmurhash/
 */
uint32_t murmurHash32(const void* key, std::size_t len, unsigned int seed);

/*!
 * \brief Murmur hash 2 64-bit for 64-bit platforms.
 * \note This code makes a few assumptions about how your machine behaves:
 *   1. We can read a 8-byte value from any address without crashing.
 * \note And it has a few limitations:
 *   1. It will not work incrementally;
 *   2. It will not produce the same results on little-endian and big-endian machines.
 *
 * Source: https://sites.google.com/site/murmurhash/
 */
uint64_t murmurHash64(const void* key, std::size_t len, unsigned int seed);

/*!
 * \brief Murmur hash 2 64-bit for 32-bit platforms.
 * \note This code makes a few assumptions about how your machine behaves:
 *   1. We can read a 8-byte value from any address without crashing.
 * \note And it has a few limitations:
 *   1. It will not work incrementally;
 *   2. It will not produce the same results on little-endian and big-endian machines.
 *
 * Source: https://sites.google.com/site/murmurhash/
 */
uint64_t murmurHash6432(const void* key, std::size_t len, unsigned int seed);

}	// namespace murmur_2
}	// namespace base
