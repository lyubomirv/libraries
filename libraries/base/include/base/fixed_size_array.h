//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cassert>
#include <cstddef>

namespace base
{

template <typename T, std::size_t Size>
class FixedSizeArray
{
public:
	typedef T value_type;
	typedef value_type* iterator;
	typedef const value_type* const_iterator;

public:
	FixedSizeArray()
		: _count(0)
	{
	}

	FixedSizeArray(std::size_t n, const value_type& value = value_type())
		: _count(0)
	{
		resize(n, value);
	}

public:
	T& operator[] (std::size_t index)
	{
		assert(index < _count);
		return _array[index];
	}

	const T& operator[] (std::size_t index) const
	{
		assert(index < _count);
		return _array[index];
	}

	const T* getArray() const
	{
		return _array;
	}

	std::size_t push(const value_type& value)
	{
		assert(_count <= Size && "Reached the limit of elements.");

		const std::size_t index = _count;
		_array[index] = value;

		++_count;

		return index;
	}

	void eraseOrdered(iterator it)
	{
		eraseOrdered(it - _array);
	}

	void eraseOrdered(std::size_t index)
	{
		if(_count == 0)
		{
			return;
		}

		assert(index < _count && "Invalid index.");

		while(index < _count - 1)
		{
			_array[index] = _array[index + 1];
			++index;
		}

		--_count;
	}

	void eraseUnordered(iterator it)
	{
		eraseUnordered(it - _array);
	}

	void eraseUnordered(std::size_t index)
	{
		if(_count == 0)
		{
			return;
		}

		assert(index < _count && "Invalid index.");

		_array[index] = _array[_count - 1];
		--_count;
	}

	void resize(std::size_t n, const value_type& value = value_type())
	{
		assert(n <= capacity() && "Attempt to fill an array with more than its limit.");

		const std::size_t newSize = (n < capacity()) ? n : capacity();

		if(newSize > size())
		{
			for(std::size_t i = _count; i < newSize; ++i)
			{
				_array[i] = value;
			}
		}

		_count = newSize;
	}

	void clear()
	{
		_count = 0;
	}

	std::size_t size() const
	{
		return _count;
	}

	std::size_t capacity() const
	{
		return Size;
	}

public:
	iterator begin()
	{
		return &_array[0];
	}

	const_iterator begin() const
	{
		return &_array[0];
	}

	iterator end()
	{
		return &_array[_count];
	}

	const_iterator end() const
	{
		return &_array[_count];
	}

private:
	typedef value_type Array[Size];
	Array _array;
	std::size_t _count;
};

}	// namespace base
