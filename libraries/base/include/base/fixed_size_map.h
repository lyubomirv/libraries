//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cassert>
#include <algorithm>
#include <functional>

namespace base
{

template <typename Key, typename Mapped, std::size_t Size, Mapped InvalidMapped = Mapped(-1)>
class FixedSizeMap
{
public:
	typedef Key    key_type;
	typedef Mapped mapped_type;
	typedef std::pair<key_type, mapped_type> value_type;
	typedef value_type* iterator;
	typedef const value_type* const_iterator;

public:
	FixedSizeMap()
		: _mappingsCount(0)
	{
	}

public:
	void add(const key_type& key, const mapped_type& mapped)
	{
		assert(!hasKey(key) && "key already has value.");
		assert(_mappingsCount <= Size && "Reached the limit of keys for the map.");

		_mappings[_mappingsCount] = value_type(key, mapped);

		++_mappingsCount;

		std::sort(_mappings, _mappings + _mappingsCount, ValueComparator());
	}

	void set(const key_type& key, const mapped_type& mapped)
	{
		if(!hasKey(key))
		{
			add(key, mapped);
			return;
		}

		value_type* findPtr = std::find_if(
			_mappings,
			_mappings + _mappingsCount,
			KeyComparator(key));

		findPtr->second = mapped;
	}

	bool hasKey(const key_type& key) const
	{
		return std::binary_search(
			_mappings,
			_mappings + _mappingsCount,
			value_type(key, InvalidMapped),
			ValueComparator());
	}

	mapped_type get(const key_type& key) const
	{
		const value_type* findPtr = std::find_if(
			_mappings,
			_mappings + _mappingsCount,
			KeyComparator(key));

		if(findPtr == _mappings + _mappingsCount)
		{
			assert(false && "Can not find key in map.");
			return InvalidMapped;
		}

		return findPtr->second;
	}

	void erase(const key_type& key)
	{
		value_type* findPtr = std::find_if(
			_mappings,
			_mappings + _mappingsCount,
			KeyComparator(key));

		if(findPtr == _mappings + _mappingsCount)
		{
			assert(false && "Can not find key in map.");
			return;
		}

		findPtr->first = _mappings[_mappingsCount - 1].first;
		findPtr->second = _mappings[_mappingsCount - 1].second;

		--_mappingsCount;

		std::sort(_mappings, _mappings + _mappingsCount, ValueComparator());
	}

public:
	iterator begin()
	{
		return &_mappings[0];
	}

	const_iterator begin() const
	{
		return &_mappings[0];
	}

	iterator end()
	{
		return &_mappings[_mappingsCount];
	}

	const_iterator end() const
	{
		return &_mappings[_mappingsCount];
	}

private:
	typedef value_type Mappings[Size];

	Mappings _mappings;
	std::size_t _mappingsCount;

private:
	struct ValueComparator
		: public std::binary_function<value_type, value_type, bool>
	{
		bool operator() (const value_type& left, const value_type& right) const
		{
			return left.first < right.first;
		}
	};

	struct KeyComparator
		: public std::unary_function<value_type, bool>
	{
		KeyComparator(const key_type& key)
			: _key(key)
		{
		}

		bool operator() (const value_type& value) const
		{
			return _key == value.first;
		}

	private:
		KeyComparator& operator= (const KeyComparator& other);

	private:
		const key_type& _key;
	};
};

}	// namespace base
