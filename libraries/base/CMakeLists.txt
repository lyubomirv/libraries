cmake_minimum_required(VERSION 2.8)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

add_library(base STATIC
	${CMAKE_CURRENT_SOURCE_DIR}/include/base/fixed_size_array.h
	${CMAKE_CURRENT_SOURCE_DIR}/include/base/fixed_size_map.h
	${CMAKE_CURRENT_SOURCE_DIR}/include/base/murmur_hash_2.h
	${CMAKE_CURRENT_SOURCE_DIR}/include/base/platform_defines.h
	${CMAKE_CURRENT_SOURCE_DIR}/source/murmur_hash_2.cpp
)

# Export variables.
set(BASE_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include CACHE PATH "Path to the base library include directory.")
set(BASE_LIBRARY base CACHE STRING "Name of the built base library.")

if(CXXTEST_FOUND)
	execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/tests)
	execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/testgen)
	include_directories(${CXXTEST_INCLUDE_DIR})
	include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
	enable_testing()
	
	CXXTEST_ADD_TEST(fixed_size_array_test testgen/fixed_size_array_test.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/test/fixed_size_array_test.h)
	target_link_libraries(fixed_size_array_test base)

	CXXTEST_ADD_TEST(fixed_size_map_test testgen/fixed_size_map_test.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/test/fixed_size_map_test.h)
	target_link_libraries(fixed_size_map_test base)

	CXXTEST_ADD_TEST(murmur_hash_2_test testgen/murmur_hash_2_test.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/test/murmur_hash_2_test.h)
	target_link_libraries(murmur_hash_2_test base)
endif()
