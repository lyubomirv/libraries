//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "base/fixed_size_map.h"

class FixedSizeMapTest : public CxxTest::TestSuite
{
public:
	void testAdd()
	{
		base::FixedSizeMap<int, int, 10> map;
		map.add(1, 1);

		TS_ASSERT(true);
	}

	void testEmptyHasNot()
	{
		base::FixedSizeMap<int, int, 10> map;

		TS_ASSERT(!map.hasKey(1));
	}

	void testHas()
	{
		base::FixedSizeMap<int, int, 10> map;

		map.add(1, 1);

		TS_ASSERT(map.hasKey(1));
	}

	void testGet()
	{
		base::FixedSizeMap<int, int, 10> map;

		map.add(1, 2);

		TS_ASSERT_EQUALS(map.get(1), 2);
	}

	void testSetNotPresent()
	{
		base::FixedSizeMap<int, int, 10> map;
		map.set(1, 1);

		TS_ASSERT_EQUALS(map.get(1), 1);
	}

	void testSetPresent()
	{
		base::FixedSizeMap<int, int, 10> map;
		map.add(1, 1);
		map.set(1, 2);

		TS_ASSERT_EQUALS(map.get(1), 2);
	}

	void testErase()
	{
		base::FixedSizeMap<int, int, 10> map;

		map.add(1, 2);
		map.add(2, 3);

		map.erase(1);

		TS_ASSERT(!map.hasKey(1));
		TS_ASSERT(map.hasKey(2));
	}

	void testIterate()
	{
		typedef base::FixedSizeMap<int, int, 10> Map;
		Map map;

		for(int i = 0; i < 10; ++i)
		{
			map.add(i, i);
		}

		int index = 0;
		for(Map::const_iterator it = map.begin(); it != map.end(); ++it, ++index)
		{
			TS_ASSERT_EQUALS(it->first, index);
			TS_ASSERT_EQUALS(it->second, index);
		}
	}
};
