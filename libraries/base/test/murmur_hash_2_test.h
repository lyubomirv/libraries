//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "base/murmur_hash_2.h"

class MurmurHash2Test : public CxxTest::TestSuite 
{
public:
	void testHash32()
	{
		const unsigned int hash0 = base::murmur_2::murmurHash32("asd", 3, 0);
		const unsigned int hash1 = base::murmur_2::murmurHash32("fgh", 3, 0);
		
		TS_ASSERT(hash0 != hash1);
	}
};
