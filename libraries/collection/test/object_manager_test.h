//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "collection/object_manager.h"

#include "memory/allocators.h"

namespace
{
typedef collection::ObjectIdType<unsigned short, unsigned short> ObjectId;

struct Object
{
	ObjectId id;
	int x;
	float y;
	char c;
};
}

class ObjectManagerTest : public CxxTest::TestSuite
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	void testCreate()
	{
		collection::ObjectManager<ObjectId, Object> om(memory::allocators::heap());
		TS_ASSERT(true);
	}

	void testAdd()
	{
		collection::ObjectManager<ObjectId, Object> om(memory::allocators::heap());
		TS_ASSERT_EQUALS(om.objects().size(), 0);
		om.add();
		TS_ASSERT_EQUALS(om.objects().size(), 1);
	}

	void testHasNot()
	{
		collection::ObjectManager<ObjectId, Object> om(memory::allocators::heap());
		TS_ASSERT(!om.has(ObjectId()));
	}

	void testHas()
	{
		collection::ObjectManager<ObjectId, Object> om(memory::allocators::heap());
		ObjectId oid = om.add();
		TS_ASSERT(om.has(oid));
	}

	void testRemove()
	{
		collection::ObjectManager<ObjectId, Object> om(memory::allocators::heap());
		ObjectId oid = om.add();
		TS_ASSERT(om.has(oid));
		om.remove(oid);
		TS_ASSERT(!om.has(oid));
	}

	void testAddRemoveAddDifferentId()
	{
		collection::ObjectManager<ObjectId, Object> om(memory::allocators::heap());
		ObjectId oid1 = om.add();
		om.remove(oid1);
		ObjectId oid2 = om.add();
		TS_ASSERT(oid1 != oid2);
	}

	void testAddRemoveMore()
	{
		collection::ObjectManager<ObjectId, Object> om(memory::allocators::heap());
		ObjectId oids[5];
		Object object;
		object.c = 'a';
		oids[0] = om.add(object);
		oids[1] = om.add();
		oids[2] = om.add();
		oids[3] = om.add();
		oids[4] = om.add();
		om.remove(oids[1]);
		om.remove(oids[3]);
		oids[3] = om.add();
		oids[1] = om.add();
		TS_ASSERT(om.has(oids[0]));
		TS_ASSERT_EQUALS(object.c, om.get(oids[0]).c);
		TS_ASSERT(om.has(oids[1]));
		TS_ASSERT(om.has(oids[2]));
		TS_ASSERT(om.has(oids[3]));
		TS_ASSERT(om.has(oids[4]));
	}
};
