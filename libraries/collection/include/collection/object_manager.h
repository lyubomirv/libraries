//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cassert>

#include "memory/i_allocator.h"
#include "mstd/vector.h"

#include "collection/object_id_type.h"

namespace collection
{

/**
 * \class ObjectManager
 * \brief Collection of objects, belonging to one subsystem.
 *
 * Provides an unique id for each objects, by which it can be identified.
 * The objects are stored linearly in memory and can be accessed directly.
 * The \a _indices collection indexes objects in the \a _objects collection.
 * The class keeps track of the freed indices in the \a _indices collection
 * in-place, by the means of a free queue (like a free list but a queue). It is
 * linked by the \a next member of the \a Index struct, and the class keeps
 * front and back indices for the queue.
 * \tparam ObjectId type of object identifier to use. Should be a specialization
 * of the template struct ObjectIdType.
 * \tparam Object type of object to store. The object must have an \a id member
 * of type \a ObjectId.
 */
template <typename ObjectId, typename Object>
class ObjectManager
{
public:
	ObjectManager(memory::IAllocator& allocator)
		: _indices(allocator)
		, _objects(allocator)
		, _freeQueueFront(-1)
		, _freeQueueBack(-1)
	{
	}

	bool has(const ObjectId id)
	{
		if(id.index >= _indices.size())
		{
			return false;
		}
		const Index& index = _indices[id.index];
		return id == index.id && index.index != size_t(-1);
	}

	Object& get(const ObjectId id)
	{
		assert(id.index <= _indices.size() && "Invalid id.");
		return _objects[_indices[id.index].index];
	}

	ObjectId add(const Object& object = Object())
	{
		// When there's no free queue, create one free index.
		if(_freeQueueFront == size_t(-1))
		{
			Index index;
			index.id.identity = 0;
			index.id.index = static_cast<typename ObjectId::index_type>(_indices.size());
			index.next = size_t(-1);
			_indices.push_back(index);
			_freeQueueFront = _indices.size() - 1;
		}

		// When we're at the end of the free queue, clear it.
		if(_freeQueueFront == _freeQueueBack)
		{
			_freeQueueBack = size_t(-1);
		}

		Index& index = _indices[_freeQueueFront];
		_freeQueueFront = index.next;
		index.id.identity += 1;
		index.index = _objects.size();
		_objects.push_back(object);
		_objects.back().id = index.id;
		return index.id;
	}

	void remove(const ObjectId id)
	{
		assert(id.index <= _indices.size() && "Invalid id.");
		Index& index = _indices[id.index];

		Object& o = _objects[index.index];
		o = _objects.back();
		_indices[o.id.index].index = index.index;

		index.index = -1;
		// When there's no free queue, set its front.
		// Otherwise, just extend it.
		if(_freeQueueBack == size_t(-1))
		{
			_freeQueueFront = id.index;
		}
		else
		{
			_indices[_freeQueueBack].next = id.index;
		}
		_freeQueueBack = id.index;
	}

public:
	typedef mstd::Vector<Object> Objects;

	const Objects& objects() const
	{
		return _objects;
	}

private:
	struct Index
	{
		ObjectId id;
		size_t index;
		size_t next;
	};
	typedef mstd::Vector<Index> Indices;
	Indices _indices;
	Objects _objects;
	size_t _freeQueueFront; // Take from here
	size_t _freeQueueBack;  // Put free here
};

}	// namespace collection
