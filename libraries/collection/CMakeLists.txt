cmake_minimum_required(VERSION 2.8)

include_directories(
	${MEMORY_INCLUDE_DIR}
	${MSTD_INCLUDE_DIR}
	${CMAKE_CURRENT_SOURCE_DIR}/include)

# Export variables.
set(COLLECTION_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include CACHE PATH "Path to the collection library include directory.")

if(CXXTEST_FOUND)
	execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/tests)
	execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/testgen)
	include_directories(${CXXTEST_INCLUDE_DIR})
	include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
	include_directories(${BASE_INCLUDE_DIR})
	include_directories(${MEMORY_INCLUDE_DIR})
	include_directories(${MSTD_INCLUDE_DIR})
	enable_testing()

	CXXTEST_ADD_TEST(object_manager_test testgen/object_manager_test.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/test/object_manager_test.h)
	target_link_libraries(object_manager_test ${MEMORY_LIBRARY})
endif()
