//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

namespace input
{

namespace event
{
enum Type
{
	Action,
	Range,
	State
};
}	// namespace event

typedef unsigned int EventId;

/*!
 * \struct Event
 * \brief Base event data. Contains the id of the event.
 */
struct Event
{
	EventId id;
};

struct Action : public Event
{
};

struct Range : public Event
{
	/*!
	 * \brief The current position.
	 *
	 * A value in the range [-100, 100].
	 */
	short position;

	/*!
	 * \brief The difference from the previous position.
	 */
	short delta;

	/*!
	 * \brief Minimal value for this range.
	 *
	 * This value depends on the input that is responsible for this range.
	 * It can be 0 for events, caused by mouse or -100 for events, caused by
	 * a joystick axis.
	 */
	short min;
	/*!
	 * \brief Maximal value for this range.
	 *
	 * This value depends on the input that is responsible for this range.
	 * It can be the width/height of the screen/window for events, caused by
	 * mouse or +100 for events, caused by a joystick axis.
	 */
	short max;
};

struct State : public Event
{
};

/*!
 * \brief Creates an event of type \a EventType. Initializes it with
 * the given \a eventId.
 */
template <typename EventType>
inline EventType createEvent(const EventId& eventId)
{
	EventType event;
	event.id = eventId;
	return event;
}

}	// namespace input
