//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "base/fixed_size_array.h"
#include "base/fixed_size_map.h"
#include "input/context.h"
#include "input/event.h"
#include "input/keyboard.h"
#include "input/mouse.h"

namespace input
{

/*!
 * \class InputSystem
 * \brief Controls the input system.
 */
class InputSystem
{
public:
	InputSystem();

	/*!
	 * \brief Updates the state of the input system. Call this on every frame.
	 *
	 * \param deltaTime Time since last frame in seconds.
	 */
	void update(float deltaTime);

public:
	void registerAction(const EventId& eventId);
	void registerRepeatedAction(const EventId& eventId, std::size_t repeatRateMilliseconds);
	void registerRange(const EventId& eventId);
	void registerState(const EventId& eventId);

public:
	typedef std::size_t ContextId;

	/*!
	 * \brief Adds a context to the input system.
	 * \return the identificator, that was assigned on the context.
	 */
	ContextId addContext(const Context& context);

	/*!
	 * \brief Sets the context as active.
	 *
	 * There may be more than one contexts active at a time.
	 * \note: Active context mappings must \a not overlap.
	 */
	void setContextActive(ContextId contextId);

	/*!
	 * \brief Sets the context as inactive.
	 *
	 * It is possible that there is no active context at the moment. In such
	 * cases, input is simply not processed.
	 */
	void setContextInactive(ContextId contextId);

public:
	void setScreenSize(unsigned int width, unsigned int height);

public:
	void keyboardKeyPressed(const keyboard::Key& key);
	void keyboardKeyReleased(const keyboard::Key& key);
	void mouseButtonPressed(const mouse::Button& button);
	void mouseButtonReleased(const mouse::Button& button);
	void mouseMoved(short x, short y);
	void mouseWheelMoved(short delta);

public:
	/*!
	 * \brief Clears all generated events.
	 *
	 * Clears the actions and the ranges. The states stay until the input that
	 * triggered them is released.
	 * \note This is automatically called on each call to \a update but can be
	 * called manually if needed.
	 */
	void clearEvents();

	std::size_t getActionsCount() const;
	const Action* getActions() const;

	std::size_t getRangesCount() const;
	const Range* getRanges() const;

	std::size_t getStatesCount() const;
	const State* getStates() const;

private:
	template <typename KeyType>
	void keyPressed(const KeyType& key);

	template <typename KeyType>
	void keyReleased(const KeyType& key);

	template <typename AxisType>
	void axisMoved(const AxisType& axis, short position, short min, short max);

private:
	enum
	{
		MAX_CONTEXTS = 5
	};

	typedef base::FixedSizeArray<Context, MAX_CONTEXTS> Contexts;
	Contexts _contexts;

	typedef base::FixedSizeArray<bool, MAX_CONTEXTS> ContextActive;
	ContextActive _contextActive;

private:
	void registerEventType(const EventId& eventId, event::Type type);
	bool isRepeatedAction(const EventId& eventId) const;
	void clearContextEvents(const ContextId& contextId);
	bool doesContextOverlapWithActive(const ContextId& contextId) const;

	enum
	{
		MAX_EVENTS = 256,
		MAX_REPEATED_ACTIONS = 128
	};

	typedef base::FixedSizeMap<EventId, event::Type, MAX_EVENTS> EventTypes;
	EventTypes _eventTypes;

	typedef base::FixedSizeMap<EventId, std::size_t, MAX_REPEATED_ACTIONS> RepeatedActionRates;
	RepeatedActionRates _repeatedActionRates;

private:
	typedef base::FixedSizeMap<EventId, std::size_t, MAX_REPEATED_ACTIONS> RepeatedActionsLastTime;
	RepeatedActionsLastTime _repeatedActionsLastTime;

	std::size_t _totalTimeMilliseconds;

private:
	enum
	{
		MAX_ACTIVE_INPUT = 48
	};

	// Keeps track of all the active input (pressed keys, etc.)
	typedef base::FixedSizeArray<Context::InputId, MAX_ACTIVE_INPUT> ActiveInput;
	ActiveInput _activeInput;

	enum
	{
		MAX_AXES_COUNT = 64
	};

	// Keeps the current position of each axis ever used. For delta calculation.
	typedef base::FixedSizeMap<Context::InputId, short, MAX_AXES_COUNT> AxisPosition;
	AxisPosition _axisPosition;

private:
	unsigned int _screenWidth;
	unsigned int _screenHeight;

private:
	enum
	{
		MAX_ACTIVE_ACTIONS = 16,
		MAX_ACTIVE_RANGES = 16,
		MAX_ACTIVE_STATES = 16
	};

	typedef base::FixedSizeArray<Action, MAX_ACTIVE_ACTIONS> Actions;
	typedef base::FixedSizeArray<Range, MAX_ACTIVE_ACTIONS> Ranges;
	typedef base::FixedSizeArray<State, MAX_ACTIVE_ACTIONS> States;

	Actions _actions;
	Ranges _ranges;
	States _states;
};

}	// namespace input
