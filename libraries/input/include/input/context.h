//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <utility>

#include "base/fixed_size_map.h"
#include "input/event.h"
#include "input/keyboard.h"
#include "input/mouse.h"

namespace input
{

/*!
 * \class Context
 * \brief Represents an input mapping context.
 *
 * Contexts can be switched by the input system. There can be multiple
 * contexts, that are active at any moment but they should not overlap
 * their mappings.
 */
class Context
{
public:
	void addMapping(keyboard::Key key, EventId eventId);
	bool hasMapping(keyboard::Key key) const;
	EventId getMapping(keyboard::Key key) const;

	void addMapping(mouse::Button button, EventId eventId);
	bool hasMapping(mouse::Button button) const;
	EventId getMapping(mouse::Button button) const;

	void addMapping(mouse::Axis axis, EventId eventId);
	bool hasMapping(mouse::Axis axis) const;
	EventId getMapping(mouse::Axis axis) const;

public:
	typedef unsigned int InputId;

	enum
	{
		MAX_MAPPINGS = 256
	};

	typedef base::FixedSizeMap<InputId, EventId, MAX_MAPPINGS> Mappings;

public:
	bool hasMapping(const InputId& inputId) const;

	bool isEventMapped(const EventId& eventId) const;
	const Mappings& getMappings() const;

private:
	Mappings _map;
};

}	// namespace input
