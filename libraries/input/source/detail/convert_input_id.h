//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "input/context.h"

namespace input
{
namespace detail
{

inline Context::InputId getInputId(keyboard::Key key)
{
	return static_cast<Context::InputId>(key);
}

inline Context::InputId getInputId(mouse::Button button)
{
	return static_cast<Context::InputId>(keyboard::KeyCount + button);
}

inline Context::InputId getInputId(mouse::Axis axis)
{
	return static_cast<Context::InputId>(keyboard::KeyCount + mouse::ButtonCount + axis);
}

}	// namespace detail
}	// namespace input
