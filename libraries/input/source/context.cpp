//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "input/context.h"

#include "detail/convert_input_id.h"

namespace input
{

void Context::addMapping(keyboard::Key key, EventId eventId)
{
	_map.add(detail::getInputId(key), eventId);
}

bool Context::hasMapping(keyboard::Key key) const
{
	return _map.hasKey(detail::getInputId(key));
}

EventId Context::getMapping(keyboard::Key key) const
{
	return _map.get(detail::getInputId(key));
}

void Context::addMapping(mouse::Button button, EventId eventId)
{
	_map.add(detail::getInputId(button), eventId);
}

bool Context::hasMapping(mouse::Button button) const
{
	return _map.hasKey(detail::getInputId(button));
}

EventId Context::getMapping(mouse::Button button) const
{
	return _map.get(detail::getInputId(button));
}

void Context::addMapping(mouse::Axis axis, EventId eventId)
{
	_map.add(detail::getInputId(axis), eventId);
}

bool Context::hasMapping(mouse::Axis axis) const
{
	return _map.hasKey(detail::getInputId(axis));
}

EventId Context::getMapping(mouse::Axis axis) const
{
	return _map.get(detail::getInputId(axis));
}

bool Context::hasMapping(const InputId& inputId) const
{
	return _map.hasKey(inputId);
}

bool Context::isEventMapped(const EventId& eventId) const
{
	for(Mappings::const_iterator it = _map.begin(); it != _map.end(); ++it)
	{
		const EventId& id = it->second;
		if(id == eventId)
		{
			return true;
		}
	}

	return false;
}

const Context::Mappings& Context::getMappings() const
{
	return _map;
}

}	// namespace input
