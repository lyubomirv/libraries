//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cstddef>

namespace memory
{

/*!
 * \class IAllocator
 * \brief Base allocator interface for all allocators.
 *
 * Provides functionality for tracking the total amount of
 * allocated memory. All implementers have to call
 * onAllocate and onDeallocate for proper tracking.
 */
class IAllocator
{
public:
	IAllocator();
	virtual ~IAllocator();

	/*!
	 * \brief Allocates \a size amount of memory, aligned by \a alignment.
	 *
	 * \param size the requested size of memory
	 * \param alignment the returned pointer will be a multiple of this
	 */
	virtual void* allocate(size_t size, size_t alignment) = 0;

	/*!
	 * \brief Deallocates the allocated memory, pointed by \a ptr.
	 */
	virtual void deallocate(void* ptr) = 0;

	/*!
	 * \brief Returns the size of the allocated memory, pointed by \a ptr.
	 */
	virtual size_t size(void* ptr) = 0;

	/*!
	 * \brief Returns the total allocated memory size by this allocator.
	 */
	virtual size_t totalSize() const;

protected:
	/*!
	 * \brief Performs actions on allocation
	 *
	 * Increments the allocated size.
	 */
	void onAllocate(size_t size);
	
	/*!
	 * \brief Performs actions on deallocation
	 *
	 * Decrements the allocated size.
	 */
	void onDeallocate(size_t size);

private:
	size_t _allocatedSize;
};

}	// namespace memory
