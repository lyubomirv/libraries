//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "base/platform_defines.h"

namespace memory
{
namespace detail
{

/*!
 * \brief Calls allocate on an allocator reference.
 */
template <typename Allocator>
void* allocate(Allocator& allocator, size_t size, size_t alignment)
{
	return allocator.allocate(size, alignment);
}

/*!
 * \brief Calls allocate on a pointer to allocator.
 */
template <typename Allocator>
void* allocate(Allocator* allocator, size_t size, size_t alignment)
{
	return allocator->allocate(size, alignment);
}

/*!
 * \brief Calls deallocate on an allocator reference.
 */
template <typename Allocator>
void deallocate(Allocator& allocator, void* ptr)
{
	return allocator.deallocate(ptr);
}

/*!
 * \brief Calls deallocate on a pointer to allocator.
 */
template <typename Allocator>
void deallocate(Allocator* allocator, void* ptr)
{
	return allocator->deallocate(ptr);
}

/*!
 * \brief Calls an object's destructor
 *
 * This helper function is needed because calling
 * the destuctor of a simple type explicitly is not possible.
 * So for example:
 *    int* a = ...;
 *    a->~int(); // This is not allowed.
 * Using a template of the int (or typedef -ing it) and
 * using the template type (or typedefed type) is allowed,
 * as that is then considered a named type.
 */
template <typename Type>
void destructType(Type* obj)
{
#if defined(COMPILER_MSVC)
	// Suppress compiler warning
	// c4100: unreferenced formal parameter
	// that is actually a bug of the compiler.
	obj;
#endif
	obj->~Type();
}

/*!
 * \brief Calls the destructor of the object and deallocates the memory.
 */
template <typename Allocator, typename Type>
void deleteObject(Allocator& allocator, Type* obj)
{
	destructType(obj);
	deallocate(allocator, obj);
}

/*!
 * \brief Calls the destructor of the object and deallocates the memory.
 */
template <typename Allocator, typename Type>
void deleteObject(Allocator* allocator, Type* obj)
{
	destructType(obj);
	deallocate(allocator, obj);
}

/*!
 * \brief Calls the destructors of all elements in an array.
 */
template <typename Type>
void destructArray(Type* arr, size_t elementsCount)
{
	for(size_t i = 0; i < elementsCount; ++i)
	{
		arr[i].~Type();
	}
}

/*!
 * \brief Calls the destructors of the array elements and deallocates the memory.
 */
template <typename Allocator, typename Type>
void deleteArray(Allocator& allocator, Type* arr)
{
	destructArray(arr, allocator.size(arr) / sizeof(Type));
	deallocate(allocator, arr);
}

/*!
 * \brief Calls the destructors of the array elements and deallocates the memory.
 */
template <typename Allocator, typename Type>
void deleteArray(Allocator* allocator, Type* arr)
{
	destructArray(arr, allocator->size(arr) / sizeof(Type));
	deallocate(allocator, arr);
}

}	// namespace detail
}	// namespace memory

#define MEMORY_MAX(A, B) ((A) > (B)) ? (A) : (B)

// A wrapper implementation alignof()
#if defined(COMPILER_GCC)
// On linux, the value for alignment must be at least sizeof(void*)
#	define ALIGNOF(Type) MEMORY_MAX(__alignof__(Type), sizeof(void*))
#elif defined(COMPILER_MSVC)
#	define ALIGNOF(Type) __alignof(Type)
#endif

/*!
 * \brief Allocates memory
 *
 * Takes care of the size and alignment.
 */
#define MEMALLOCATE(Allocator, Type) \
	memory::detail::allocate(Allocator, sizeof(Type), ALIGNOF(Type))

/*!
 * \brief Deallocates memory
 */
#define MEMDEALLOCATE(Allocator, Ptr) \
	memory::detail::deallocate(Allocator, Ptr)

/*!
 * \brief Allocates memory and calls placement new.
 */
#define MEMNEW(Allocator, Type) \
	new (MEMALLOCATE(Allocator, Type)) Type

/*!
 * \brief Calls destructor and deallocates memory.
 */
#define MEMDELETE(Allocator, Ptr) \
	memory::detail::deleteObject(Allocator, Ptr)

/*!
 * \brief Allocates memory for an array and calls placement new
 * to call the elements' constructors.
 */
#define MEMNEWARRAY(Allocator, Type, ElementsCount) \
	new(                                    \
		memory::detail::allocate(           \
			Allocator,                      \
			sizeof(Type) * ElementsCount,   \
			ALIGNOF(Type)))                 \
	Type[ElementsCount]

#define MEMDELETEARRAY(Allocator, Ptr) \
	memory::detail::deleteArray(Allocator, Ptr)
