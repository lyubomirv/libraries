//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "memory/i_allocator.h"

namespace memory
{

/*!
 * \class RegionAllocatorFixed
 * \brief Allocates regions of memory
 * 
 * Allocates a region of memory with a size of \a regionSize.
 * Then when asked, gives consequtive memory addresses with a fixed
 * size from that region. All requested allocations must be of a
 * predefined size.
 * When the region is depleted, it creates a new region and stores a
 * link to it.
 * No deallocations are done until the layer is destroyed.
 * All of the regions are deallocated in the layer's destructor.
 */
class RegionAllocatorFixed : public IAllocator
{
public:
	RegionAllocatorFixed(
		IAllocator& baseAllocator,
		size_t regionSize,
		size_t allocationSize);
	
	/*!
	 * \brief Destroys all allocated regions.
	 */
	virtual ~RegionAllocatorFixed();
	
	/*!
	 * \brief Returns an allocated address.
	 * 
	 * Returns the next consequtive address. Allocates a new region if needed.
	 */
	virtual void* allocate(size_t size, size_t alignment);
	
	/*!
	 * \brief Does not do anything.
	 *
	 * All memory is deallocated on destruction of the layer.
	 */
	virtual void deallocate(void*);

	/*!
	 * \brief Returns a constant, as a single allocation does not take any memory.
	 */
	virtual size_t size(void* ptr);

private:
	RegionAllocatorFixed(const RegionAllocatorFixed&);
	RegionAllocatorFixed& operator= (const RegionAllocatorFixed&);

private:
	IAllocator& _baseAllocator;

	struct Region
	{
		Region* next;
		void* currentPtr;
	};
	
	Region* _currentRegion;
	
	const size_t _regionSize;
	const size_t _allocationSize;
};

}	// namespace memory
