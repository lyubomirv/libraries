//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "base/platform_defines.h"

#if defined(PLATFORM_WINDOWS)

#include <windows.h>

namespace memory
{
namespace detail
{

class LockWindows
{
public:
	LockWindows()
	{
		InitializeCriticalSectionAndSpinCount(
			&_criticalSection, 
			0x00000400);
	}
	
	~LockWindows()
	{
		DeleteCriticalSection(&_criticalSection);
	}
	
	void lock()
	{
		EnterCriticalSection(&_criticalSection);
	}
	
	void unlock()
	{
		LeaveCriticalSection(&_criticalSection);
	}
	
private:
	CRITICAL_SECTION _criticalSection;
};

}	// namespace detail
}	// namespace memory

#endif	// PLATFORM_WINDOWS
