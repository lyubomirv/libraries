//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cassert>
#include <cstring>

#include "memory/i_allocator.h"
#include "memory/memory.h"

namespace memory
{
namespace detail
{

template <typename Key>
extern size_t hash(Key key);

template <>
inline size_t hash(void* ptr)
{
	return reinterpret_cast<size_t>(ptr);
}

template <>
inline size_t hash(const void* ptr)
{
	return reinterpret_cast<size_t>(ptr);
}

/*!
 * \class HashMap
 * \brief A hash map implementation
 *
 * This is a simple implementation of a hash map. It uses an array of lists
 * as bins (number of bins is set at creation time). Each bin is a linked list
 * of key->value pairs.
 */
template <typename Key, typename Value, size_t Size = 511>
class HashMap
{
public:
	typedef Key key_type;
	typedef Value value_type;

public:
	HashMap(IAllocator& allocator)
		: _allocator(allocator)
	{
		const size_t allocatedSize = sizeof(ListNode*) * Size;
		memset(_bins, 0, allocatedSize);
	}
	
	~HashMap()
	{
		for(size_t i = 0; i < Size; ++i)
		{
			ListNode* listNode = _bins[i];
			while(listNode != NULL)
			{
				ListNode* toDelete = listNode;
				listNode = listNode->next;
				MEMDELETE(_allocator, toDelete);
			}
		}
	}
	
	/*!
	 * \brief Set a key to its value
	 *
	 * Inserts the key if not already present
	 * \param key the key
	 * \param value the value
	 */
	void set(const key_type& key, const value_type& value)
	{
		const size_t index = getIndex(key);
		
		ListNode* listNode = _bins[index];
		
		while(listNode != NULL)
		{
			if(listNode->key == key)
			{
				listNode->value = value;
				return;
			}
			
			listNode = listNode->next;
		}
		
		insert(index, key, value);
	}
	
	/*!
	 * \brief Check if the key is in the map
	 *
	 * \param key the key
	 */
	bool has(const key_type& key) const
	{
		const ListNode* listNode = _bins[getIndex(key)];
		
		while(listNode != NULL)
		{
			if(listNode->key == key)
			{
				return true;
			}
			
			listNode = listNode->next;
		}
		
		return false;
	}
	
	/*!
	 * \brief Get the value for a key
	 *
	 * The method will assert if the key is not present in Debug and
	 * will return a default value in Release.
	 * \param key the key
	 */
	value_type get(const key_type& key) const
	{
		const ListNode* listNode = _bins[getIndex(key)];
		
		while(listNode != NULL)
		{
			if(listNode->key == key)
			{
				return listNode->value;
			}
			
			listNode = listNode->next;
		}
		
		assert(false);
		return value_type();
	}
	
	/*!
	 * \brief Erase a key from the map
	 *
	 * \param key the key
	 */
	void erase(const key_type& key)
	{
		const size_t index = getIndex(key);
		
		ListNode* listNode = _bins[index];
		ListNode* prevNode = NULL;
		while(listNode != NULL)
		{
			if(listNode->key == key)
			{
				if(prevNode != NULL)
				{
					prevNode->next = listNode->next;
				}
				else
				{
					_bins[index] = listNode->next;
				}
				
				MEMDELETE(_allocator, listNode);
				
				return;
			}
			
			prevNode = listNode;
			listNode = listNode->next;
		}
	}

private:
	HashMap(const HashMap&);
	HashMap& operator= (const HashMap&);

private:
	size_t getIndex(const key_type& key) const
	{
		return hash(key) % Size;
	}
	
	void insert(size_t index, const key_type& key, const value_type& value)
	{
		ListNode* listNode = MEMNEW(_allocator, ListNode) (key, value);
		listNode->next = _bins[index];
		_bins[index] = listNode;
	}

private:
	/*!
	 * \struct ListNode
	 * \brief Represents a list node for the lists in the buckets
	 */
	struct ListNode
	{
		ListNode(const key_type& _key, const value_type& _value)
			: key(_key)
			, value(_value)
			, next(NULL)
		{
		}
		
		key_type key;
		value_type value;
		ListNode* next;
	};
	
	// The hash map bins
	ListNode* _bins[Size];
	// Allocator to use when allocating bins and list nodes
	IAllocator& _allocator;

public:
	enum
	{
		NODE_SIZE = sizeof(ListNode)
	};

public:
	/*!
	 * \class HashMapIterator
	 * \brief Iterator class for the HashMap
	 *
	 * The iterator is a simplified version of an STL
	 * iterator. It can be used to iterate over the
	 * hash map in a for-cycle in the same way an
	 * std::vector would be iterated.
	 */
	class HashMapIterator
	{
	public:
		HashMapIterator(const HashMap& hashMap)
			: _hashMap(hashMap)
			, _currentIndex(Size)
			, _currentListNode(NULL)
		{
		}
		
		HashMapIterator(
			const HashMap& hashMap,
			size_t index,
			const typename HashMap::ListNode* listNode)
			: _hashMap(hashMap)
			, _currentIndex(index)
			, _currentListNode(listNode)
		{
		}
		
		/*!
		 * \brief Gets the key, associated with the current node
		 */
		const typename HashMap::key_type& key() const
		{
			return _currentListNode->key;
		}
		
		/*!
		 * \brief Gets the value, associated with the current node
		 */
		const typename HashMap::value_type& value() const
		{
			return _currentListNode->value;
		}
		
		bool operator== (const HashMapIterator& other) const
		{
			return _currentIndex == other._currentIndex
				&& _currentListNode == other._currentListNode;
		}
		
		bool operator!= (const HashMapIterator& other) const
		{
			return !operator==(other);
		}
		
		/*!
		 * \brief Finds the next node in the hash map.
		 *
		 * The order of looking for the next node is the following:
		 *     - if the current list node has a valid next node,
		 *       it is selected;
		 *     - otherwise, it looks in sequence for the next index
		 *       in the hash map's bins collection, which has a valid node.
		 */
		HashMapIterator& operator++ ()
		{
			if(_currentListNode != NULL)
			{
				_currentListNode = _currentListNode->next;
			}
			
			if(_currentListNode == NULL)
			{
				for(size_t i = _currentIndex + 1; i < Size; ++i)
				{
					if(_hashMap._bins[i] != NULL)
					{
						_currentIndex = i;
						_currentListNode = _hashMap._bins[i];
						return *this;
					}
				}
				
				_currentIndex = Size;
			}
			
			return *this;
		}
	
	private:
		HashMapIterator& operator= (const HashMapIterator&);
		
	private:
		const HashMap& _hashMap;
		size_t _currentIndex;
		const typename HashMap::ListNode* _currentListNode;
	};
	
public:
	typedef HashMapIterator const_iterator;

public:
	/*!
	 * \brief Checks whether the hash map is empty.
	 */
	bool empty() const
	{
		for(size_t i = 0; i < Size; ++i)
		{
			if(_bins[i] != NULL)
			{
				return false;
			}
		}
		
		return true;
	}
	
	/*!
	 * \brief Gets an iterator to the first element or an end iterator.
	 */
	const_iterator begin() const
	{
		for(size_t i = 0; i < Size; ++i)
		{
			if(_bins[i] != NULL)
			{
				return const_iterator(*this, i, _bins[i]);
			}
		}
		
		return end();
	}
	
	/*!
	 * \brief Get an end iterator to the hash map.
	 *
	 * The end iterator points an index that is one past the
	 * hash map's _bins last element. Its ListNode pointer is NULL.
	 */
	const_iterator end() const
	{
		return const_iterator(*this, Size, NULL);
	}
};

}	// namespace detail
}	// namespace memory
