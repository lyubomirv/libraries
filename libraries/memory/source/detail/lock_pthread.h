//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "base/platform_defines.h"

#if defined(PLATFORM_LINUX) || defined(PLATFORM_OSX)

#include <pthread.h>

namespace memory
{
namespace detail
{

class LockPthread
{
public:
	LockPthread()
	{
		pthread_mutex_init(&_mutex, NULL);
	}
	
	~LockPthread()
	{
		pthread_mutex_destroy(&_mutex);
	}
	
	void lock()
	{
		pthread_mutex_lock(&_mutex);
	}
	
	void unlock()
	{
		pthread_mutex_unlock(&_mutex);
	}
	
private:
	pthread_mutex_t _mutex;
};

}	// namespace detail
}	// namespace memory

#endif	// PLATFORM_LINUX || PLATFORM_OSX
