//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "base/platform_defines.h"

#if defined(PLATFORM_LINUX) || defined(PLATFORM_OSX)
#	include "lock_pthread.h"
#elif defined(PLATFORM_WINDOWS)
#	include "lock_windows.h"
#endif

namespace memory
{
namespace detail
{

#if defined(PLATFORM_LINUX) || defined(PLATFORM_OSX)

class Lock : public LockPthread
{
};

#elif defined(PLATFORM_WINDOWS)

class Lock : public LockWindows
{
};

#endif

}	// namespace detail
}	// namespace memory
