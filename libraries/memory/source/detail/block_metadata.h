//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

namespace memory
{
namespace detail
{

template <typename H>
inline size_t sizeWithPadding(size_t size, size_t alignment)
{
	return size + alignment + sizeof(H);
}

template <typename H>
inline void* dataPointer(H* header, size_t alignment)
{
	void* data = header + 1;

	size_t address = reinterpret_cast<size_t>(data);
	const size_t mod = address % alignment;
	if(mod != 0)
	{
		address += (alignment - mod);
	}

	return reinterpret_cast<void*>(address);
}

/**
 * \brief Gets pointer to the location of the header, given pointer to the data.
 */
template <typename H>
inline H* headerPointer(void* data)
{
	H* header = static_cast<H*>(data);
	while(header[-1].value == typename H::type(-1))
	{
		--header;
	}
	return header - 1;
}

template <typename H>
inline void fill(H* header, void* data, size_t value)
{
	header->value = static_cast<typename H::type>(value);
	while(++header < data)
	{
		header->value = typename H::type(-1);
	}
}

}	// namespace detail
}	// namespace memory
