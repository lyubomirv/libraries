//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "memory/i_allocator.h"

namespace memory
{

/*!
 * \class PageAllocator
 * \brief Allocates memory by using the system's memory pages
 * allocating utilities.
 */
class PageAllocator: public IAllocator
{
public:
	PageAllocator();

	virtual void* allocate(size_t size, size_t alignment);
	virtual void deallocate(void* ptr);

	virtual size_t size(void* ptr);

private:
	size_t _systemPageSize;
};

}	// namespace memory
