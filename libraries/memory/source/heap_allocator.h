//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "memory/i_allocator.h"

namespace memory
{

/*!
 * \class HeapAllocator
 * \brief Allocates memory by using the system's heap allocating utility.
 */
class HeapAllocator: public IAllocator
{
public:
	HeapAllocator();
	virtual ~HeapAllocator();

	virtual void* allocate(size_t size, size_t alignment);
	virtual void deallocate(void* ptr);

	virtual size_t size(void* ptr);
	virtual size_t totalSize() const;
};

}	// namespace memory
