//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "heap_allocator.h"

#include <cassert>
#include <new>

#include "base/platform_defines.h"

#if defined(PLATFORM_LINUX)

#include <cstdlib>
#include <malloc.h>

namespace memory
{

HeapAllocator::HeapAllocator()
{
}

HeapAllocator::~HeapAllocator()
{
}

void* HeapAllocator::allocate(size_t size, size_t alignment)
{
	void* ptr = NULL;
	int result = posix_memalign(&ptr, alignment, size);
	assert(result == 0);

	const size_t allocatedSize = this->size(ptr);
	onAllocate(allocatedSize);

	return ptr;
}

void HeapAllocator::deallocate(void* ptr)
{
	const size_t allocatedSize = size(ptr);
	onDeallocate(allocatedSize);

	::free(ptr);
}

size_t HeapAllocator::size(void* ptr)
{
	return ::malloc_usable_size(ptr);
}

size_t HeapAllocator::totalSize() const
{
	return IAllocator::totalSize();
}

}	// namespace memory

#elif defined(PLATFORM_OSX)

#include <cstdlib>
#include <malloc/malloc.h>

namespace memory
{

HeapAllocator::HeapAllocator()
{
}

HeapAllocator::~HeapAllocator()
{
}

void* HeapAllocator::allocate(size_t size, size_t)
{
	void* ptr = NULL;
	// posix_memalign in OSX is present in versions >= 10.6.
	// OSX's malloc by default returns 16 byte aligned memory.
	ptr = ::malloc(size);

	const size_t allocatedSize = this->size(ptr);
	onAllocate(allocatedSize);

	return ptr;
}

void HeapAllocator::deallocate(void* ptr)
{
	const size_t allocatedSize = size(ptr);
	onDeallocate(allocatedSize);

	::free(ptr);
}

size_t HeapAllocator::size(void* ptr)
{
	return ::malloc_size(ptr);
}

size_t HeapAllocator::totalSize() const
{
	return IAllocator::totalSize();
}

}	// namespace memory

#elif defined(PLATFORM_WINDOWS)

#include <malloc.h>
#include "detail/block_metadata.h"

namespace
{

/**
 * \struct AlignmentHeader
 * \brief Stored at the beginning of an allocation to track its alignment.
 */
struct AlignmentHeader
{
	typedef short type;
	type value;
};

}	// namespace

namespace memory
{

HeapAllocator::HeapAllocator()
{
}

HeapAllocator::~HeapAllocator()
{
}

void* HeapAllocator::allocate(size_t size, size_t alignment)
{
	const size_t totalSize = detail::sizeWithPadding<AlignmentHeader>(size, alignment);
	AlignmentHeader* header = static_cast<AlignmentHeader*>(
		::_aligned_malloc(totalSize, alignment));
	assert(header != NULL);
	void* ptr = detail::dataPointer(header, alignment);
	detail::fill(header, ptr, alignment);

	const size_t allocatedSize = this->size(ptr);
	onAllocate(allocatedSize);

	return ptr;
}

void HeapAllocator::deallocate(void* ptr)
{
	const size_t allocatedSize = size(ptr);
	onDeallocate(allocatedSize);

	AlignmentHeader* header = detail::headerPointer<AlignmentHeader>(ptr);
	::_aligned_free(header);
}

size_t HeapAllocator::size(void* ptr)
{
	AlignmentHeader* header = detail::headerPointer<AlignmentHeader>(ptr);
	return ::_aligned_msize(header, header->value, 0);
}

size_t HeapAllocator::totalSize() const
{
	return IAllocator::totalSize();
}

}	// namespace memory

#endif	// PLATFORM_WINDOWS
