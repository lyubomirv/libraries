//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "memory/region_allocator_fixed.h"

#include <cassert>

namespace memory
{

//////////////////////////////////////////////////////////////////
// Helpers

/*!
 * \brief Align a pointer to an address, that is the next multiple of \a alignment.
 * \param ptr the original pointer
 * \param alignment the wanted alignment
 * \return the aligned pointer
 */
static void* align(void* ptr, size_t alignment)
{
	size_t address = reinterpret_cast<size_t>(ptr);
	size_t newAddress = (address + (alignment - 1)) & (~(alignment - 1));
	return reinterpret_cast<void*>(newAddress);
}

/*!
 * \brief Tells if there is at least \a size free space in the region,
 * starting from the address, pointed by \a ptr.
 */
static bool hasSpace(
	const void* regionStart,
	size_t regionSize,
	void* ptr,
	size_t size)
{
	return static_cast<const char*>(ptr) + size
		<= reinterpret_cast<const char*>(regionStart) + regionSize;
}

//////////////////////////////////////////////////////////////////
// RegionAllocatorFixed

RegionAllocatorFixed::RegionAllocatorFixed(
	IAllocator& baseAllocator,
	size_t regionSize,
	size_t allocationSize)
	: _baseAllocator(baseAllocator)
	, _currentRegion(NULL)
	, _regionSize(regionSize)
	, _allocationSize(allocationSize)
{
}

RegionAllocatorFixed::~RegionAllocatorFixed()
{
	Region* region = _currentRegion;
	while(region != NULL)
	{
		Region* toDelete = region;
		region = region->next;
		_baseAllocator.deallocate(toDelete);
		
		onDeallocate(_regionSize);
	}
}

void* RegionAllocatorFixed::allocate(size_t size, size_t alignment)
{
	if(size != _allocationSize)
	{
		assert(false);
		return NULL;
	}
	
	void* ptr = (_currentRegion != NULL)
		? align(_currentRegion->currentPtr, alignment)
		: NULL;
	if(ptr == NULL || !hasSpace(_currentRegion, _regionSize, ptr, size))
	{
		Region* oldRegion = _currentRegion;
		
		_currentRegion = static_cast<Region*>(
			_baseAllocator.allocate(_regionSize, sizeof(void*)));
		_currentRegion->next = oldRegion;
		
		onAllocate(_regionSize);
		
		ptr = align(reinterpret_cast<char*>(_currentRegion) + sizeof(Region), alignment);
		_currentRegion->currentPtr = static_cast<char*>(ptr) + size;
		
		return ptr;
	}
	
	_currentRegion->currentPtr = static_cast<char*>(ptr) + size;
	
	return ptr;
}

void RegionAllocatorFixed::deallocate(void*)
{
}

size_t RegionAllocatorFixed::size(void*)
{
	return _allocationSize;
}

}	// namespace memory
