//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "memory/debug_allocator.h"

#include <cassert>
#include <cstdio>
#include <new>

#include "page_allocator.h"
#include "memory/region_allocator_fixed.h"
#include "memory/free_list_allocator.h"
#include "detail/hash_map.h"
#include "detail/lock.h"

namespace memory
{

static const size_t HASH_MAP_REGION_SIZE = 16384;

///////////////////////////////////////////////////
// DebugAllocatorImpl

class DebugAllocator::DebugAllocatorImpl
{
public:
	DebugAllocatorImpl(IAllocator& baseAllocator)
		: _baseAllocator(baseAllocator)
		, _hashMapRegionAllocator(
			_hashMapPageAllocator,
			HASH_MAP_REGION_SIZE,
			PointerInfoMap::NODE_SIZE)
		, _hashMapFreeListAllocator(_hashMapRegionAllocator)
		, _pointerInfo(_hashMapFreeListAllocator)
	{
	}
	
	/*!
	 * \brief Checks if all allocations are deallocated
	 */
	~DebugAllocatorImpl()
	{
		for(PointerInfoMap::const_iterator it = _pointerInfo.begin(); it != _pointerInfo.end(); ++it)
		{
			const PointerInfo& ptrInfo = it.value();
			
			if(ptrInfo.state != PointerInfo::FREED)
			{
				printf(
					"Pointer not deallocated, allocated at %s: %d\n",
					ptrInfo.file,
					ptrInfo.line);
				assert(false && NOT_DEALLOCATED);
				return;
			}
		}
	}
	
	/*!
	 * \brief Allocates memory
	 *
	 * Performs all relevant checks and stores some information for the allocation.
	 * \param file the file, where the allocation was made
	 * \param line the line in the source, where the allocation was made
	 */
	void* allocate(size_t size, size_t alignment, const char* file, unsigned short line)
	{
		void* ptr = _baseAllocator.allocate(size, alignment);
		
		_pointerInfoLock.lock();
		
		if(_pointerInfo.has(ptr))
		{
			PointerInfo ptrInfo = _pointerInfo.get(ptr);
			
			if(ptrInfo.state == PointerInfo::ALLOCATED)
			{
				printf(
					"Allocate returned an already allocated address, allocated at %s: %d\n",
					ptrInfo.file,
					ptrInfo.line);
				assert(false && ALREADY_ALLOCATED);
				return NULL;
			}
		}
		
		_pointerInfo.set(ptr, PointerInfo(file, line, PointerInfo::ALLOCATED));
		
		_pointerInfoLock.unlock();
		
		return ptr;
	}
	
	/*!
	 * \brief Overrides allocate with only the size and alignment arguments
	 *
	 * Forwards the call to allocate by supplying a default file and line.
	 */
	void* allocate(size_t size, size_t alignment)
	{
		return allocate(size, alignment, "Unknown", 0);
	}
	
	/*!
	 * \brief Dellocates memory
	 *
	 * Performs all relevant checks.
	 */
	void deallocate(void* ptr)
	{
		_pointerInfoLock.lock();
		
		if(!_pointerInfo.has(ptr))
		{
			assert(false && NOT_ALLOCATED);
			return;
		}
		
		PointerInfo ptrInfo = _pointerInfo.get(ptr);
		if(ptrInfo.state == PointerInfo::FREED)
		{
			printf(
				"Multiple dellocates for the same pointer, allocated at %s: %d\n",
				ptrInfo.file,
				ptrInfo.line);
			assert(false && ALREADY_FREED);
			return;
		}
		
		ptrInfo.state = PointerInfo::FREED;
		_pointerInfo.set(ptr, ptrInfo);
		
		_pointerInfoLock.unlock();
		
		_baseAllocator.deallocate(ptr);
	}

	size_t size(void* ptr)
	{
		return _baseAllocator.size(ptr);
	}
	
	size_t totalSize() const
	{
		return _hashMapFreeListAllocator.totalSize();
	}

private:
	IAllocator& _baseAllocator;

	/*!
	 * \struct PointerInfo
	 * \brief Stores information for an allocation.
	 */
	struct PointerInfo
	{
		PointerInfo()
			: line(0)
			, state(FREED)
			, file(NULL)
		{
		}
		
		PointerInfo(
			const char* file_,
			unsigned short line_,
			unsigned short state_)
			: line(line_)
			, state(state_)
			, file(file_)
		{
		}
		
		bool operator== (const PointerInfo& other) const
		{
			return file == other.file
				&& line == other.line
				&& state == other.state;
		}
		
		static const unsigned short ALLOCATED = 1;
		static const unsigned short FREED = 0;
		
		unsigned short line;
		unsigned short state;
		const char* file;
	};
	
	PageAllocator _hashMapPageAllocator;
	RegionAllocatorFixed _hashMapRegionAllocator;
	FreeListAllocator _hashMapFreeListAllocator;
	
	// Stores information for all allocations
	typedef detail::HashMap<void*, PointerInfo> PointerInfoMap;
	PointerInfoMap _pointerInfo;
	
	detail::Lock _pointerInfoLock;
	
	enum Errors
	{
		ALREADY_ALLOCATED,
		NOT_ALLOCATED,
		ALREADY_FREED,
		NOT_DEALLOCATED
	};
};

///////////////////////////////////////////////////
// DebugAllocator

DebugAllocator::DebugAllocator(IAllocator& baseAllocator)
{
	assert(sizeof(DebugAllocatorImpl) <= sizeof(_implBuffer));
	_impl = new (static_cast<void*>(_implBuffer)) DebugAllocatorImpl(baseAllocator);
}

DebugAllocator::~DebugAllocator()
{
	_impl->~DebugAllocatorImpl();
	_impl = NULL;
}

void* DebugAllocator::allocate(size_t size, size_t alignment)
{
	void* ptr = _impl->allocate(size, alignment);
	
	const size_t allocatedSize = _impl->size(ptr);
	onAllocate(allocatedSize);
	
	return ptr;
}

void DebugAllocator::deallocate(void* ptr)
{
	const size_t allocatedSize = _impl->size(ptr);
	onDeallocate(allocatedSize);
	
	_impl->deallocate(ptr);
}

size_t DebugAllocator::size(void* ptr)
{
	return _impl->size(ptr);
}

size_t DebugAllocator::totalSize() const
{
	return IAllocator::totalSize() + _impl->totalSize();
}

void* DebugAllocator::allocate(size_t size, size_t alignment, const char* file, unsigned short line)
{
	void* ptr = _impl->allocate(size, alignment, file, line);
	
	const size_t allocatedSize = _impl->size(ptr);
	onAllocate(allocatedSize);
	
	return ptr;
}

}	// namespace memory
