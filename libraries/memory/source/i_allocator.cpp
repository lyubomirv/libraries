//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "memory/i_allocator.h"

#include <cassert>

namespace memory
{

IAllocator::IAllocator()
	: _allocatedSize(0)
{
}

IAllocator::~IAllocator()
{
	assert(_allocatedSize == 0);
}

size_t IAllocator::totalSize() const
{
	return _allocatedSize;
}

void IAllocator::onAllocate(size_t size)
{
	_allocatedSize += size;
}

void IAllocator::onDeallocate(size_t size)
{
	_allocatedSize -= size;
}

}	// namespace memory
