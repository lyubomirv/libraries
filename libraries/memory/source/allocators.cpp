//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "memory/allocators.h"

#include <cassert>
#include <new>

#include "heap_allocator.h"
#include "page_allocator.h"

namespace
{

struct MemoryGlobal
{
	MemoryGlobal()
		: heapAllocator(NULL)
		, pageAllocator(NULL)
	{
	}

	memory::HeapAllocator* heapAllocator;
	memory::PageAllocator* pageAllocator;

	static const size_t ALLOCATORS_SIZE =
		sizeof(memory::HeapAllocator)
		+ sizeof(memory::PageAllocator);
	char buffer[ALLOCATORS_SIZE];
};

MemoryGlobal memoryGlobal;

}	// namespace

namespace memory
{
namespace allocators
{

void create()
{
	assert(memoryGlobal.heapAllocator == NULL
		&& memoryGlobal.pageAllocator == NULL
		&& "Memory allocators already created.");

	char* p = memoryGlobal.buffer;
	memoryGlobal.heapAllocator = new(p) memory::HeapAllocator();
	p += sizeof(memory::HeapAllocator);
	memoryGlobal.pageAllocator = new(p) memory::PageAllocator();
}

void destroy()
{
	assert(memoryGlobal.heapAllocator != NULL
		&& memoryGlobal.pageAllocator != NULL
		&& "Memory allocators not created.");

	memoryGlobal.heapAllocator->~HeapAllocator();
	memoryGlobal.pageAllocator->~PageAllocator();
	memoryGlobal = MemoryGlobal();
}

IAllocator& heap()
{
	assert(memoryGlobal.heapAllocator != NULL && "Memory allocators not created.");

	return *memoryGlobal.heapAllocator;
}

IAllocator& page()
{
	assert(memoryGlobal.pageAllocator != NULL && "Memory allocators not created.");

	return *memoryGlobal.pageAllocator;
}

}	// namespace allocators
}	// namespace memory
