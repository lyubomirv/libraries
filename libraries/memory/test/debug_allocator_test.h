//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "memory/debug_allocator.h"
#include "memory/allocators.h"
#include "memory/memory.h"

class DebugAllocatorTest : public CxxTest::TestSuite 
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	void testAllocHeap()
	{
		memory::DebugAllocator allocator(memory::allocators::heap());
		void* memory = allocator.allocate(sizeof(int), ALIGNOF(int), __FILE__, __LINE__);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);
		
		allocator.deallocate(memory);
	}
	
	void testAllocPage()
	{
		memory::DebugAllocator allocator(memory::allocators::page());
		void* memory = allocator.allocate(sizeof(int), ALIGNOF(int), __FILE__, __LINE__);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);
		
		allocator.deallocate(memory);
		// TODO: The assert doesn not come from the debug allocator
		// if we try to deallocate twice here (using heap allocator works nice)
		//allocator.deallocate(memory);
	}
	
	void testSize()
	{
		memory::DebugAllocator allocator(memory::allocators::heap());
		void* memory = allocator.allocate(sizeof(int), ALIGNOF(int), __FILE__, __LINE__);
		
		TS_ASSERT(allocator.size(memory) >= sizeof(int));
		
		allocator.deallocate(memory);
	}
	
	void testTotalSize()
	{
		memory::DebugAllocator allocator(memory::allocators::heap());
		void* memory = allocator.allocate(sizeof(int), ALIGNOF(int), __FILE__, __LINE__);
		
		TS_ASSERT(allocator.totalSize() >= sizeof(int));
		
		allocator.deallocate(memory);
	}
	
	void testFree()
	{
		memory::DebugAllocator allocator(memory::allocators::heap());
		void* memory = allocator.allocate(1000000, 8, __FILE__, __LINE__);
		
		allocator.deallocate(memory);
	}
};
