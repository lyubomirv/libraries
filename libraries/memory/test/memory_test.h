//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "memory/allocators.h"
#include "memory/memory.h"

class MemoryTest : public CxxTest::TestSuite 
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	void testAllocate()
	{
		void* memory = MEMALLOCATE(memory::allocators::heap(), int);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);
		
		MEMDEALLOCATE(memory::allocators::heap(), memory);
	}
	
	void testNew()
	{
		int* i = MEMNEW(memory::allocators::heap(), int) (1);
		
		TS_ASSERT_EQUALS(*i, 1);
		
		MEMDELETE(memory::allocators::heap(), i);
	}
	
	void testNewArray()
	{
		int* arr = MEMNEWARRAY(memory::allocators::heap(), int, 10);
		
		for(size_t i = 0; i < 10; ++i)
		{
			int& a = arr[i];
			a = 5;
		}
		
		MEMDELETEARRAY(memory::allocators::heap(), arr);
	}
};
