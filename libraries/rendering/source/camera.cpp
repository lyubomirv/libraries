//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "rendering/camera.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

namespace rendering
{

Camera::Camera()
	: _position(0.0f, 0.0f, 1.0f)
	, _lookAt(0.0f, 0.0f, -1.0f)
	, _up(0.0f, 1.0f, 0.0f)
	, _fovy(45.0f)
	, _aspect(4.0f / 3.0f)
	, _zNear(0.1f)
	, _zFar(100.0f)
{
	recreateProjectionView();
}

void Camera::setPerspective(float fovy, float aspect, float zNear, float zFar)
{
	_fovy = fovy;
	_aspect = aspect;
	_zNear = zNear;
	_zFar = zFar;

	recreateProjectionView();
}

void Camera::setPosition(const glm::vec3& position)
{
	_position = position;
	recreateProjectionView();
}

void Camera::setLookAt(const glm::vec3& lookAt)
{
	_lookAt = lookAt;
	recreateProjectionView();
}

void Camera::setUp(const glm::vec3& up)
{
	_up = up;
	recreateProjectionView();
}

void Camera::move(const glm::vec3& direction)
{
	_position += direction;
	recreateProjectionView();
}

void Camera::rotate(const glm::vec3& around, float radians)
{
	_lookAt = glm::rotate(_lookAt, radians, around);
	_up = glm::rotate(_up, radians, around);
	recreateProjectionView();
}

const glm::mat4& Camera::projectionView() const
{
	return _projectionView;
}

const glm::vec3& Camera::up() const
{
	return _up;
}

const glm::vec3& Camera::lookAt() const
{
	return _lookAt;
}

void Camera::recreateProjectionView()
{
	const glm::mat4 projection = glm::perspective(_fovy, _aspect, _zNear, _zFar);
	const glm::mat4 view = glm::lookAt(_position, _position + _lookAt, _up);
	_projectionView = projection * view;
}

}	// namespace rendering
