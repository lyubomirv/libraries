//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "rendering/vertex_array_object_manager.h"

#include <GL/glew.h>

#include "memory/allocators.h"

#include "rendering/opengl.h"

namespace rendering
{

VertexArrayObjectManager::VertexArrayObjectManager()
	: _vaoIds(memory::allocators::heap())
{
}

VertexArrayObjectManager::~VertexArrayObjectManager()
{
	for(VAOIds::const_iterator it = _vaoIds.begin(); it != _vaoIds.end(); ++it)
	{
		remove(*it);
	}
}

VAOId VertexArrayObjectManager::generate()
{
	VAOId vaoId = 0;
	glGenVertexArrays(1, &vaoId);
	_vaoIds.push_back(vaoId);

	return vaoId;
}

void VertexArrayObjectManager::addArrayBuffer(
	VAOId vaoId,
	BufferId bufferId,
	unsigned int index,
	unsigned int sizePerElement,
	BufferElementType bfeType)
{
	GLenum type = 0;
	switch(bfeType)
	{
		case BFE_FLOAT:
			type = GL_FLOAT;
			break;
	}

	glBindVertexArray(vaoId);

	glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, sizePerElement, type, GL_FALSE, 0, NULL);

	glBindVertexArray(0);
}

void VertexArrayObjectManager::addElementBuffer(VAOId vaoId, BufferId bufferId)
{
	glBindVertexArray(vaoId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);
	glBindVertexArray(0);
}

void VertexArrayObjectManager::remove(VAOId vaoId)
{
	glDeleteVertexArrays(1, &vaoId);
}

}	// namespace rendering
