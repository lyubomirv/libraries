//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "rendering/texture_manager.h"

#include <cassert>

#include <GL/glew.h>

#include "memory/allocators.h"

#include "rendering/opengl.h"
#include "detail/bmp_load.h"

namespace rendering
{

TextureManager::TextureManager()
	: _textures(memory::allocators::heap())
{
}

TextureManager::~TextureManager()
{
	for(TextureIds::const_iterator it = _textures.begin(); it != _textures.end(); ++it)
	{
		remove(*it);
	}
}

TextureId TextureManager::generate()
{
	GLuint textureId;
	glGenTextures(1, &textureId);

	_textures.push_back(textureId);

	return textureId;
}

bool TextureManager::create(
	TextureId textureId,
	texture::Type type,
	const unsigned char* filedata,
	unsigned int length)
{
	mstd::Vector<unsigned char> textureData(memory::allocators::heap(), length, 0);

	GLint internalFormat = GL_RGB;
	GLenum format = GL_RGB;

	unsigned int width = 0;
	unsigned int height = 0;

	bool result = true;

	switch(type)
	{
		case texture::BMP:
			result = detail::getTextureDataBMP(
				filedata,
				length,
				&textureData[0],
				width,
				height);
			internalFormat = GL_RGB;
			format = GL_BGR;
			break;

		default:
			assert(false && "Unhandled texture type.");
			return false;
	}

	if(!result)
	{
		return false;
	}

	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, &textureData[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}

void TextureManager::remove(TextureId textureId)
{
	glDeleteTextures(1, &textureId);

	/// \note Texture not removed from store. Think about it.
}

}	// namespace rendering
