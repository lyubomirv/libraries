//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "rendering/shader_program_manager.h"

#include <iostream>

#include <GL/glew.h>

#include "memory/allocators.h"

#include "rendering/opengl.h"

namespace rendering
{

ShaderProgramManager::ShaderProgramManager()
	: _shaderProgramIds(memory::allocators::heap())
{
}

ShaderProgramManager::~ShaderProgramManager()
{
	for(ShaderProgramIds::const_iterator it = _shaderProgramIds.begin(); it != _shaderProgramIds.end(); ++it)
	{
		remove(*it);
	}
}

ShaderProgramId ShaderProgramManager::generate()
{
	ShaderProgramId shaderProgramId = glCreateProgram();
	_shaderProgramIds.push_back(shaderProgramId);

	return shaderProgramId;
}

void ShaderProgramManager::attachShader(
	ShaderProgramId shaderProgramId,
	ShaderId shaderId)
{
	glAttachShader(shaderProgramId, shaderId);
}

bool ShaderProgramManager::create(ShaderProgramId shaderProgramId)
{
	glLinkProgram(shaderProgramId);

	// Check program linking.
	GLint result = GL_FALSE;
	glGetProgramiv(shaderProgramId, GL_LINK_STATUS, &result);
	if(result != GL_TRUE)
	{
		const GLsizei MAX_MESSAGE_LENGTH = 1024;
		char message[MAX_MESSAGE_LENGTH];
		GLsizei length;
		glGetProgramInfoLog(shaderProgramId, MAX_MESSAGE_LENGTH, &length, message);

		/// \todo Logging.
		std::cerr
			<< "Shader program compilation failed for program: " << shaderProgramId
			<< ". Log message: " << message
			<< std::endl;

		return false;
	}

	return true;
}

void ShaderProgramManager::remove(ShaderProgramId shaderProgramId)
{
	glDeleteProgram(shaderProgramId);
}

}	// namespace rendering
