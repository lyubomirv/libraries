//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "mstd/vector.h"

#include "rendering/types.h"

namespace rendering
{

class ShaderProgramManager
{
public:
	ShaderProgramManager();
	~ShaderProgramManager();

public:
	ShaderProgramId generate();
	void attachShader(ShaderProgramId shaderProgramId, ShaderId shaderId);
	bool create(ShaderProgramId shaderProgramId);
	void remove(ShaderProgramId shaderProgramId);

private:
	typedef mstd::Vector<ShaderProgramId> ShaderProgramIds;
	ShaderProgramIds _shaderProgramIds;
};

}	// namespace rendering
