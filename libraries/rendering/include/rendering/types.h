//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <GL/glew.h>
#include "collection/object_id_type.h"
#include "rendering/opengl.h"

namespace rendering
{

typedef GLuint BufferId;
typedef GLuint ShaderId;
typedef GLuint ShaderProgramId;
typedef GLuint TextureId;
typedef GLuint VAOId;
typedef collection::ObjectIdType<unsigned short, unsigned short> ObjectId;

}	// namespace rendering
