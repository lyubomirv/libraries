//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <glm/glm.hpp>

namespace rendering
{

class Camera
{
public:
	Camera();

	void setPerspective(
		float fovy,
		float aspect,
		float zNear = 0.1f,
		float zFar = 100.0f);
	void setPosition(const glm::vec3& position);
	void setLookAt(const glm::vec3& lookAt);
	void setUp(const glm::vec3& up);

	void move(const glm::vec3& direction);
	void rotate(const glm::vec3& around, float radians);

	const glm::mat4& projectionView() const;
	const glm::vec3& up() const;
	const glm::vec3& lookAt() const;

private:
	void recreateProjectionView();

private:
	glm::mat4 _projectionView;

	glm::vec3 _position;
	glm::vec3 _lookAt;
	glm::vec3 _up;

	float _fovy;
	float _aspect;
	float _zNear;
	float _zFar;
};

}	// namespace rendering
