//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "mstd/vector.h"

#include "rendering/types.h"

namespace rendering
{

namespace texture
{
enum Type
{
	BMP
};
}

class TextureManager
{
public:
	TextureManager();
	~TextureManager();

public:
	TextureId generate();
	bool create(
		TextureId textureId,
		texture::Type type,
		const unsigned char* filedata,
		unsigned int length);
	void remove(TextureId textureId);

private:
	typedef mstd::Vector<TextureId> TextureIds;
	TextureIds _textures;
};

}	// namespace rendering
