//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "mstd/vector.h"

#include "rendering/types.h"

namespace rendering
{

class BufferManager
{
public:
	BufferManager();
	~BufferManager();

public:
	enum BufferType
	{
		BT_ARRAY,
		BT_ELEMENT
	};

public:
	BufferId generate();
	bool create(BufferId bufferId, BufferType type, const void* data, unsigned int byteCount);
	void remove(BufferId bufferId);

private:
	typedef mstd::Vector<BufferId> BufferIds;
	BufferIds _bufferIds;
};

}	// namespace rendering
