//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "collection/object_manager.h"
#include "rendering/camera.h"
#include "rendering/rendering_data.h"
#include "rendering/types.h"
#include "rendering/buffer_manager.h"
#include "rendering/shader_manager.h"
#include "rendering/shader_program_manager.h"
#include "rendering/texture_manager.h"
#include "rendering/vertex_array_object_manager.h"

namespace rendering
{

class RenderingSystem
{
public:
	typedef collection::ObjectManager<ObjectId, RenderingData> Objects;

public:
	RenderingSystem();

	bool initialize(unsigned int width, unsigned int height);

	void resize(unsigned int width, unsigned int height);
	void draw();

	Objects& objects();
	Camera& camera();

	BufferManager& bufferManager();
	ShaderManager& shaderManager();
	ShaderProgramManager& shaderProgramManager();
	TextureManager& textureManager();
	VertexArrayObjectManager& vaoManager();

private:
	Objects _objects;
	Camera _camera;

	BufferManager _bufferManager;
	ShaderManager _shaderManager;
	ShaderProgramManager _shaderProgramManager;
	TextureManager _textureManager;
	VertexArrayObjectManager _vaoManager;
};

}	// namespace rendering
