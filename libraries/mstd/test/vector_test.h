//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "mstd/vector.h"

#include "memory/allocators.h"

class VectorTest : public CxxTest::TestSuite 
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	void testCreateDefault()
	{
		mstd::Vector<int> vec(memory::allocators::heap());
		
		TS_ASSERT(vec.empty());
	}
	
	void testCreateSize()
	{
		mstd::Vector<int> vec(memory::allocators::heap(), 10);
		
		TS_ASSERT_EQUALS(vec.size(), 10);
	}
	
	void testCreateIterators()
	{
		mstd::Vector<int> vec1(memory::allocators::heap(), 10);
		
		mstd::Vector<int> vec2(memory::allocators::heap(), vec1.begin(), vec1.end());
		
		TS_ASSERT_EQUALS(vec2.size(), 10);
	}
	
	void testCopyConstruct()
	{
		mstd::Vector<int> vec1(memory::allocators::heap(), 10, 1);
		
		mstd::Vector<int> vec2(vec1);
		
		TS_ASSERT_EQUALS(vec2.size(), 10);
		
		for(size_t i = 0; i < vec2.size(); ++i)
		{
			TS_ASSERT_EQUALS(vec2[i], 1);
		}
	}
	
	void testCopyConstructWithAnotherAllocator()
	{
		mstd::Vector<int> vec1(memory::allocators::heap(), 10, 1);
		
		mstd::Vector<int> vec2(memory::allocators::heap(), vec1);
		
		TS_ASSERT_EQUALS(vec2.size(), 10);
		
		for(size_t i = 0; i < vec2.size(); ++i)
		{
			TS_ASSERT_EQUALS(vec2[i], 1);
		}
	}
	
	void testAssignmentOperator()
	{
		mstd::Vector<int> vec1(memory::allocators::heap(), 10, 1);
		
		mstd::Vector<int> vec2(memory::allocators::heap());
		vec2 = vec1;
		
		TS_ASSERT_EQUALS(vec2.size(), 10);
		
		for(size_t i = 0; i < vec2.size(); ++i)
		{
			TS_ASSERT_EQUALS(vec2[i], 1);
		}
	}
	
	void testStructConstruct()
	{
		mstd::Vector<IntWrapper> vec(memory::allocators::heap(), 10, IntWrapper());
		
		TS_ASSERT_EQUALS(vec.size(), 10);
		
		for(size_t i = 0; i < vec.size(); ++i)
		{
			TS_ASSERT_EQUALS(vec[i].wrapped, 1);
		}
	}
	
	void testIterators()
	{
		mstd::Vector<IntWrapper> vec(memory::allocators::heap(), 10, IntWrapper());
		
		TS_ASSERT_EQUALS(vec.size(), 10);
		
		for(mstd::Vector<IntWrapper>::const_iterator it = vec.begin(); it != vec.end(); ++it)
		{
			const IntWrapper& intWrapper = *it;
			
			TS_ASSERT_EQUALS(intWrapper.wrapped, 1);
		}
	}

private:
	struct IntWrapper
	{
		IntWrapper()
			: wrapped(1)
		{
		}
		
		int wrapped;
	};
};
