//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <vector>

#include <cxxtest/TestSuite.h>

#include "mstd/std_allocator.h"

#include "memory/allocators.h"
#include "memory/memory.h"

class StdAllocatorTest : public CxxTest::TestSuite 
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	void testAllocate()
	{
		mstd::StdAllocator<int> allocator(memory::allocators::heap());
		
		int* i = allocator.allocate(1);
		
		TS_ASSERT(i);
		
		allocator.deallocate(i, 1);
	}
	
	void testConstruct()
	{
		mstd::StdAllocator<int> allocator(memory::allocators::heap());
		
		int* i = static_cast<int*>(MEMALLOCATE(memory::allocators::heap(), int));
		
		allocator.construct(i, 1);
		
		TS_ASSERT_EQUALS(*i, 1);
		
		allocator.destroy(i);
		
		MEMDEALLOCATE(memory::allocators::heap(), i);
	}
	
	void testConstructArray()
	{
		mstd::StdAllocator<int> allocator(memory::allocators::heap());
		
		int* arr = allocator.allocate(10);
		for(size_t i = 0; i < 10; ++i)
		{
			allocator.construct(&arr[i], 1);
		}
		
		for(size_t i = 0; i < 10; ++i)
		{
			TS_ASSERT_EQUALS(arr[i], 1);
		}
		
		for(size_t i = 0; i < 10; ++i)
		{
			allocator.destroy(&arr[i]);
		}
		
		allocator.deallocate(arr, 10);
	}
	
	void testVectorInt()
	{
		mstd::StdAllocator<int> allocator(memory::allocators::heap());
		std::vector<int, mstd::StdAllocator<int> > vec(allocator);
		
		vec.push_back(1);
		vec.push_back(1);
		vec.push_back(1);
		vec.push_back(2);
	}
	
	void testVectorStruct()
	{
		mstd::StdAllocator<IntWrapper> allocator(memory::allocators::heap());
		std::vector<IntWrapper, mstd::StdAllocator<IntWrapper> > vec(10, IntWrapper(), allocator);
		
		for(size_t i = 0; i < 10; ++i)
		{
			TS_ASSERT_EQUALS(vec[i].wrapped, 1);
		}
	}

private:
	struct IntWrapper
	{
		IntWrapper()
			: wrapped(1)
		{
		}
		
		int wrapped;
	};
};
