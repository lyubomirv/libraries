//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <map>

#include "mstd/std_allocator.h"

#include "memory/i_allocator.h"

namespace mstd
{

/*!
 * \class Map
 * \brief Memory-aware map.
 *
 * Inherits from the stl map class.
 * Requires that an allocator is passed in its constructor.
 */
template <class Key, class T, class Compare = std::less<Key> >
class Map : public std::map<Key, T, Compare, StdAllocator<std::pair<const Key, T> > >
{
private:
	typedef StdAllocator<std::pair<const Key, T> > allocator_type;
	typedef std::map<Key, T, Compare, allocator_type> base_type;

public:
	explicit Map(memory::IAllocator& allocator, const Compare& comp = Compare())
		: base_type(comp, allocator_type(allocator))
	{
	}
	
	template <class InputIterator>
	Map(
		memory::IAllocator& allocator,
		InputIterator first,
		InputIterator last,
		const Compare& comp = Compare())
		: base_type(first, last, comp, allocator_type(allocator))
	{
	}
	
	Map(const Map& other)
		: base_type(other)
	{
	}
	
	Map(memory::IAllocator& allocator, const Map& other)
		: base_type(
			other.begin(),
			other.end(),
			other.key_comp(),
			allocator_type(allocator))
	{
	}
	
	Map& operator= (const Map& other)
	{
		if(this != &other)
		{
			base_type::operator= (other);
		}
		
		return *this;
	}
};

}	// namespace mstd
