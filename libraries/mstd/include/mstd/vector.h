//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <vector>

#include "mstd/std_allocator.h"

#include "memory/i_allocator.h"

namespace mstd
{

/*!
 * \class Vector
 * \brief Memory-aware vector
 *
 * Inherits from the stl vector class.
 * Requires that an allocator is passed in its constructor.
 */
template <class T>
class Vector : public std::vector<T, StdAllocator<T> >
{
private:
	typedef StdAllocator<T> allocator_type;
	typedef std::vector<T, allocator_type> base_type;

public:
	explicit Vector(memory::IAllocator& allocator)
		: base_type(allocator_type(allocator))
	{
	}
	
	explicit Vector(
		memory::IAllocator& allocator,
		typename base_type::size_type n,
		const typename base_type::value_type& value= T())
		: base_type(n, value, allocator_type(allocator))
	{
	}
	
	template <class InputIterator>
	Vector(memory::IAllocator& allocator, InputIterator first, InputIterator last)
		: base_type(first, last, allocator_type(allocator))
	{
	}
	
	Vector(const Vector& other)
		: base_type(other)
	{
	}
	
	Vector(memory::IAllocator& allocator, const Vector& other)
		: base_type(other.begin(), other.end(), allocator_type(allocator))
	{
	}
	
	Vector<T>& operator= (const Vector<T>& other)
	{
		if(this != &other)
		{
			base_type::operator= (other);
		}
		
		return *this;
	}
};

}	// namespace mstd
