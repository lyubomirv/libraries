cmake_minimum_required(VERSION 2.8)

include_directories(
	${BASE_INCLUDE_DIR})

# Export variables.
set(MSTD_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include CACHE PATH "Path to the mstd library include directory.")

if(CXXTEST_FOUND)
	execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/tests)
	execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/testgen)
	include_directories(${CXXTEST_INCLUDE_DIR})
	include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
	include_directories(${MEMORY_INCLUDE_DIR})
	enable_testing()
	
	CXXTEST_ADD_TEST(std_allocator_test testgen/std_allocator_test.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/test/std_allocator_test.h)
	target_link_libraries(std_allocator_test memory)
	
	CXXTEST_ADD_TEST(vector_test testgen/vector_test.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/test/vector_test.h)
	target_link_libraries(vector_test memory)
	
	CXXTEST_ADD_TEST(map_test testgen/map_test.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/test/map_test.h)
	target_link_libraries(map_test ${MEMORY_LIBRARY})
endif()
