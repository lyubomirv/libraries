libraries
---------

Collection of C++ libraries. Currently, these are the available ones:

* base - platform/compiler/etc. definitions, basic fixed size collections (array, map);
* memory - memory management library. Provides access to the system's heap and page allocators and has implementations of several other ones that can sit on top of the system ones - Region, Freelist, Debug allocators;
* mstd - memory-aware std library. Provides wrappers for some of the std collections (std::vector, std::map, etc.), that use the *memory* library for allocating but take the allocator in their constructor, instead of as a template parameter, which is the standard way of the standard library;
* collection - provides other collections, like the *ObjectManager*, which is a very useful storage for objects that can be identified via an id. They are at the same time kept linearly in memory but can be relocated without breaking their id;
* input - input managing library. Provides means to map keys, buttons, axis to events, all organized in contexts that can be enabled and disabled;
* rendering - simple rendering layer, hiding actual opengl calls but doing nothing more realy (for now).

### Setting up development environment

Linux/OSX:

* Install g++;
* Install cmake;
* Install python

Windows:

* Install Microsoft Windows SDK 7.1;
* Install Python;
* Install CMake.

### Building

To build the libraries and the test executables, you need to:

    mkdir build
    cd build
    cmake ..
    make
When you need to compile for 32bit architecture, you need to add to the `cmake ..` command an additional argument: `-DARCHITECTURE_BITS_64=OFF`.

If you then want to run the tests:

    make test
